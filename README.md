# Wildbuddy leveling profiles #

## Goals 
* Profiles that are as 'AFK-able' as possible given the current maturity level of the Wildbuddy product.
* Profiles that are intelligent enough to support path missions, challenges, and area events
* Profiles that are intelligent enough to attempt to collect the needed lore objects
* Profiles that are intelligent enough to collect all of the required resources needed for Settlers

## Caveats

* Early, and I mean damn early **BETA** versions of profiles
* Very limited support for **explorers**. Most of their path missions involve Parkour style play which is too complex for the bot to handle
* Does not support challenges that involve areas where the **Navigation Mesh** is too messy to work or requires **Parkour** style play
* **Lore** is currently unsupported until I spend time figuring out the lore code in the game engine
* **Scientist** must use the **Newton** and **Zen Scan** Addons. Find these on Curse. They're required in order to Auto-Scan mobs as you kill them.  The profiles will handle any manual scanning of stationary objects.
* **Settlers** must **MANUALLY** select the item to build at the settler station.  The profile **will collect the required resources**, move to the settler station, and open the interface.  You, the user, are required to pick an item from the list and select Build.  Hopefully we'll figure out how to automate this soon.

##Read the Following Challenge and Path Specific notes
####[Challenge Breakdown by Zone](ZoneNotes.md)
####[Mission Breakdown by Zone](ZoneMissions.md)

## Getting Set Up

* Install a GIT source control product.  I prefer [SourceTree](https://www.sourcetreeapp.com/) but your product is up to you
* create a directory on your computer to store the profiles.
* clone the repository there
* **Check for updates often!  I'm updating these files several times per day**
* Having Issues? Visit the [Issue Tracker](https://bitbucket.org/GScottSingleton/wbprofiles/issues?status=new&status=open)

### Contribution guidelines ###

* Submit a pull request.  It's that simple.


