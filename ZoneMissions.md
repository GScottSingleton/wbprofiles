# Mission Notes by Zone #

**Updated 2015.11.1**

##Ark Ship

* **Medbay Mahem** - Ranged characters tend to get stuck here as the system will target mobs in adjacent rooms. There's also a chance Melee characters will get caught in a corner once the mission is complete and the navigation system cannot get them out of the room properly.  
* **Deck Sweeper** - Real problems for ranged characters here.  on the run down to the lowest level they have a tendency to target and engage mobs located in corners without Line-of-sight.  This happens almost every single run-through I've made with a ranged unit. I quickly switch the bot to agility, run to an area with a clean LOS to several mobs, and switch back to the profile bot.

##Northern Wilds
* **Empowered Tower** - The mesh is a little messy here.  Several times I've found the character running into a boulder or stuck attempting to get to one of the ice shelves.  This is hard to avoid because it's highly variable  and depends on what mob you killed last (your start pos) and what mob the bot wants to target next.  If the route takes you directly into a small ledge or boulder you're hosed; best to just double-jump ovoer it when you see it happen.

##Algoroc
* **Finish Him!** -  The game and bot have always crashed for me when entering or exiting the ship. It's really hard to debug since it picks up fine when it gets restarted. :/
* **Crowe Homestead** -- The Mesh is really really messy here.  There are lots of fences and barricades that make navigation really trick.  Play close attention while botting here to  prevent being reported for running into a small fence for 30+ minutes straight :P 
* 



